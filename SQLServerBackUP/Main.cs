﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace SQLServerBackUP
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            SqlDataSourceEnumerator instance = SqlDataSourceEnumerator.Instance;
            System.Data.DataTable table = instance.GetDataSources();
            foreach (System.Data.DataRow row in table.Rows)
            {
                if (row["ServerName"] != DBNull.Value && Environment.MachineName.Equals(row["ServerName"].ToString()))
                {
                    string item = string.Empty;
                    item = row["ServerName"].ToString();
                    if (row["InstanceName"] != DBNull.Value || !string.IsNullOrEmpty(Convert.ToString(row["InstanceName"]).Trim()))
                    {
                        item += @"\" + Convert.ToString(row["InstanceName"]).Trim();
                    }
                    comboBox1.Items.Add(item);
                }
            }
            SqlConnectionStringBuilder b = new SqlConnectionStringBuilder(Properties.Settings.Default.Con);
            comboBox1.Text = b.DataSource.ToString();
            comboBox2.Text = b. UserInstance.ToString() ;
            txt_User.Text = b.UserID 



        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            
            txt_Pass.Enabled = txt_User.Enabled = radioButton2.Checked; 
        }
    }
}
